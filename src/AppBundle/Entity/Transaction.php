<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bookingDate", type="datetime")
     */
    private $bookingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valutaDate", type="datetime")
     */
    private $valutaDate;

    /**
     * @var string
     *
     * @ORM\Column(name="bookingText", type="string", length=255, nullable=true)
     */
    private $bookingText;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="string", length=255, nullable=true)
     */
    private $notice;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(name="documentNumber", type="integer", nullable=true)
     */
    private $documentNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="origininator", type="string", length=255, nullable=true)
     */
    private $origininator;

    /**
     * @var string
     *
     * @ORM\Column(name="originatorIban", type="string", length=34, nullable=true)
     */
    private $originatorIban;

    /**
     * @var string
     *
     * @ORM\Column(name="originatorBic", type="string", length=11, nullable=true)
     */
    private $originatorBic;

    /**
     * @var string
     *
     * @ORM\Column(name="receiverName", type="string", nullable=true)
     */
    private $receiverName;

    /**
     * @var int
     *
     * @ORM\Column(name="receiverIban", type="string", length=34, nullable=true)
     */
    private $receiverIban;

    /**
     * @var int
     *
     * @ORM\Column(name="receiverBic", type="string", length=11, nullable=true)
     */
    private $receiverBic;

    /**
     * @var string
     *
     * @ORM\Column(name="paymentCause", type="string", length=255, nullable=true)
     */
    private $paymentCause;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookingDate
     *
     * @param \DateTime $bookingDate
     *
     * @return Transaction
     */
    public function setBookingDate($bookingDate)
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    /**
     * Get bookingDate
     *
     * @return \DateTime
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * Set valutaDate
     *
     * @param \DateTime $valutaDate
     *
     * @return Transaction
     */
    public function setValutaDate($valutaDate)
    {
        $this->valutaDate = $valutaDate;

        return $this;
    }

    /**
     * Get valutaDate
     *
     * @return \DateTime
     */
    public function getValutaDate()
    {
        return $this->valutaDate;
    }

    /**
     * Set bokkingText
     *
     * @param string $bookingText
     *
     * @return Transaction
     */
    public function setBookingText($bookingText)
    {
        $this->bookingText = $bookingText;

        return $this;
    }

    /**
     * Get bokkingText
     *
     * @return string
     */
    public function getBookingText()
    {
        return $this->bookingText;
    }

    /**
     * Set notice
     *
     * @param string $notice
     *
     * @return Transaction
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Transaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set documentNumber
     *
     * @param integer $documentNumber
     *
     * @return Transaction
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }

    /**
     * Get documentNumber
     *
     * @return int
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Set origininator
     *
     * @param string $origininator
     *
     * @return Transaction
     */
    public function setOrigininator($origininator)
    {
        $this->origininator = $origininator;

        return $this;
    }

    /**
     * Get origininator
     *
     * @return string
     */
    public function getOrigininator()
    {
        return $this->origininator;
    }

    /**
     * Set originatorAccount
     *
     * @param string $originatorIban
     *
     * @return Transaction
     */
    public function setOriginatorIban($originatorIban)
    {
        $this->originatorIban = $originatorIban;

        return $this;
    }

    /**
     * Get originatorAccount
     *
     * @return string
     */
    public function getOriginatorIban()
    {
        return $this->originatorIban;
    }

    /**
     * Set originatorBic
     *
     * @param string $originatorBic
     *
     * @return Transaction
     */
    public function setOriginatorBic($originatorBic)
    {
        $this->originatorBic = $originatorBic;

        return $this;
    }

    /**
     * Get originatorBic
     *
     * @return string
     */
    public function getOriginatorBic()
    {
        return $this->originatorBic;
    }

    /**
     * Set receiverName
     *
     * @param string $receiverName
     *
     * @return Transaction
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * Get receiverName
     *
     * @return string
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * Set receiverAccount
     *
     * @param integer $receiverIban
     *
     * @return Transaction
     */
    public function setReceiverIban($receiverIban)
    {
        $this->receiverIban = $receiverIban;

        return $this;
    }

    /**
     * Get receiverAccount
     *
     * @return int
     */
    public function getReceiverIban()
    {
        return $this->receiverIban;
    }

    /**
     * Set receiverBic
     *
     * @param integer $receiverBic
     *
     * @return Transaction
     */
    public function setReceiverBic($receiverBic)
    {
        $this->receiverBic = $receiverBic;

        return $this;
    }

    /**
     * Get receiverBic
     *
     * @return int
     */
    public function getReceiverBic()
    {
        return $this->receiverBic;
    }

    /**
     * Set paymentCause
     *
     * @param string $paymentCause
     *
     * @return Transaction
     */
    public function setPaymentCause($paymentCause)
    {
        $this->paymentCause = $paymentCause;

        return $this;
    }

    /**
     * Get paymentCause
     *
     * @return string
     */
    public function getPaymentCause()
    {
        return $this->paymentCause;
    }
}
